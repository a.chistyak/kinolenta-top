let FtpDeploy = require("ftp-deploy")
let exec = require('child_process').execSync

function upload() {

    const diff = exec("cd .. && git diff --name-only") + ""
    const include = diff.length < 4 ? undefined :
        diff.split('\n').
            filter(s => s.includes("www/")).
            map(s => s.replace('www/', ''))

    let ftpDeploy = new FtpDeploy()
    let config = {
        user: "onkgroup_ftp",
        // Password optional, prompted if none given
        password: "u9L0xXn9P2",
        host: "onkgroup.ftp.tools",
        port: 21,
        localRoot: __dirname + "/../www",
        remoteRoot: "/kinolenta.top/www",
        include,
        // e.g. exclude sourcemaps, and ALL files in node_modules (including dot files)
        exclude: [
            "dist/**/*.map",
            "node_modules/**",
            "node_modules/**/.*",
            ".git/**",
            "www/storage/logs/laravel.log"
        ],
        // delete ALL existing files at destination before uploading, if true
        deleteRemote: false,
        // Passive mode is forced (EPSV command is not sent)
        forcePasv: false,
        // use sftp or ftp
        sftp: false
    }

    ftpDeploy.on("uploading", function (data) {
        console.log(data.transferredFileCount + " of " + data.totalFilesCount + " " + data.filename);
    });
    ftpDeploy.on("uploaded", function (data) {
        // console.log(data.filename + " - Done \n"); // same data as uploading event
    });
    ftpDeploy.on("upload-error", function (data) {
        console.log(data.err); // data will also include filename, relativePath, and other goodies
    });

    console.log("Deploying kinolenta.top for production...")

    ftpDeploy
        .deploy(config)
        .then(res => console.log("\nDeployed " +
            res.filter(c => c.length).length + " files successfully!"))
        .catch(err => console.log(err))
}

upload()